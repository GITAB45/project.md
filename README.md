# project.md

Буквально 5 дней назад окончательно решили на счет **обязательного** наличия регистраторов.

Закон прошел первое чтение и **по заявлению Мишустина** через неделю начнут штрафовать по всей стране за их отсутствие.

В моем регионе уже двоим знакомым пр